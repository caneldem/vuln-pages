---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults
layout: vulnerabilities
title: "Vulnerabilities"
pagination:
  enabled: true
  per_page: 10
  debug: true
  sort_field: 'date'
  sort_reverse: true
  collection: db
---

<style type="text/css" media="screen">
  .list-icon {
    width: 25px;
    height: 25px;
  }

  #search-input {
  	margin: 10px 0;
  	width: 200px;
  	height: 30px;
  }

</style>

<!-- HTML elements for search -->
<input type="text" id="search-input" placeholder="Search vulnerability with CVE">
<ul id="results-container"></ul>

<!-- or without installing anything -->
<script src="https://unpkg.com/simple-jekyll-search/dest/simple-jekyll-search.min.js"></script>

<script>
    window.simpleJekyllSearch = new SimpleJekyllSearch({
      searchInput: document.getElementById('search-input'),
      resultsContainer: document.getElementById('results-container'),
      json: '{{site.baseurl}}/search.json',
      searchResultTemplate: '<li><a href="{url}?query={query}" title="{desc}">{identifier}</a></li>',
      noResultsText: 'No results found',
      limit: 5,
      fuzzy: false,
      exclude: ['Welcome']
    })
  </script>

{% for lib in paginator.posts %}
   {% assign icon_name = "" %}
   {% if lib.package_slug contains "gem" %}
      {% assign icon_name = "assets/rubygems.svg" %}
   {% elsif lib.package_slug contains "bower" %}
      {% assign icon_name = "assets/bower.svg" %}
   {% elsif lib.package_slug contains "maven" %}
      {% assign icon_name = "assets/maven.svg" %}
   {% elsif lib.package_slug contains "npm" %}
      {% assign icon_name = "assets/npm.svg" %}
   {% elsif lib.package_slug contains "packagist" %}
      {% assign icon_name = "assets/packagist.svg" %}
   {% elsif lib.package_slug contains "pypi" %}
      {% assign icon_name = "assets/pypi.svg" %}
   {% elsif lib.package_slug contains "go" %}
      {% assign icon_name = "assets/gopher.svg" %}
   {% endif %}
  <p> <img src="{{site.baseurl}}/{{ icon_name }}" class="list-icon" /> <a href="{{site.baseurl}}/{{ lib.url }}">{{ lib.identifier }}</a> {{ lib.title }} </p>
{% endfor %}